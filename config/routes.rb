Smalltalk::Application.routes.draw do
  root 'posts#index_all'

  # settings
  get '/settings', to: redirect('/settings/profile')
  get '/settings/profile', to: 'settings#profile', as: :profile_settings
  get '/settings/favourites', to: 'settings#favourites', as: :favourites_settings

  devise_scope :user do
    get '/settings/admin', to: 'devise/registrations#edit', as: :admin_settings
  end

  resources :sections, path_names: { edit: 'settings' } do
    resources :posts, except: [:index] do
      resources :comments, only: [:create] do
        post :vote_up
      end

      member do
        post :vote_up
      end
    end

    resources :collaborations, controller: 'collaborators', only: [:create, :destroy]
    resources :favourites, only: [:create, :destroy, :update]
  end

  resources :shares, only: [:create, :new]

  # registrations controller defined so that sub-class is used
  devise_for :users, :controllers => { :registrations => :registrations }

  resources :users, only: [:show, :update]

  resources :search, only: [:index]
end
