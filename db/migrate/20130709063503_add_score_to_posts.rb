class AddScoreToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :score, :decimal, default: 0
  end
end
