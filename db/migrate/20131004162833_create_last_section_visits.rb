class CreateLastSectionVisits < ActiveRecord::Migration
  def change
    create_table :last_section_visits do |t|
      t.integer :section_id
      t.integer :user_id
      t.datetime :last_visit_at

      t.timestamps
    end
  end
end
