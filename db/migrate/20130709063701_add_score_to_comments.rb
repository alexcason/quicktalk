class AddScoreToComments < ActiveRecord::Migration
  def change
    add_column :comments, :score, :decimal, default: 0
  end
end
