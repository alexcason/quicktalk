class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.string :content
      t.integer :votes
      t.string :link
      t.references :user, index: true

      t.timestamps
    end
  end
end
