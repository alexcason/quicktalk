class AddPhoneAndLocationAndDepartmentAndCompanyToUsers < ActiveRecord::Migration
  def change
    add_column :users, :phone, :string
    add_column :users, :location, :string
    add_column :users, :department, :string
    add_column :users, :company, :string
  end
end
