class CreateCollaborators < ActiveRecord::Migration
  def change
    create_table :collaborating_sections_collaborators do |t|
      t.integer :user_id
      t.integer :section_id

      t.timestamps
    end
  end
end
