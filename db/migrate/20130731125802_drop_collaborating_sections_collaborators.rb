class DropCollaboratingSectionsCollaborators < ActiveRecord::Migration
  def up
    drop_table :collaborating_sections_collaborators
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
