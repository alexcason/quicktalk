class AddPrivateToSections < ActiveRecord::Migration
  def change
    add_column :sections, :private, :boolean, default: false
  end
end
