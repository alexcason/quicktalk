class RenameLinkToLinkUrl < ActiveRecord::Migration
  def change
  	rename_column :posts, :link, :link_url
  end
end
