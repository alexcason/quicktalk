class AddPositionToFavourites < ActiveRecord::Migration
  def change
    add_column :favourites, :position, :integer
  end
end
