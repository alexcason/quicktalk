# loops through all posts and recalculates the scores

class VoteablePostsJob
  def perform
    Post.find_each do |post|
      post.update_score
    end
  end
end