# loops through all comments for the specified post and recalculates the scores

class VoteableCommentsJob < Struct.new(:post)
  def perform
    post.comments.each do |comment|
      comment.update_score
    end
  end
end