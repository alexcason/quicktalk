class Collaboration < ActiveRecord::Base
  belongs_to :section
  belongs_to :user

  validates :section, :user, presence: true
  validates :section, uniqueness: { scope: :user, message: 'collaboration must be unique' }, :if => :section_present
  validate :collaborator_cannot_be_section_owner, :if => :section_present

  private
    def section_present
      section.present?
    end

    def collaborator_cannot_be_section_owner
      if section.user == user
        errors.add(:user, 'cannot be section owner')
      end
    end
end