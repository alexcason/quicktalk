class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :rememberable, :trackable, :validatable
  
# :recoverable, 
  
  has_many :sections
  has_many :posts
  has_many :comments

  # collaborators associations
  has_many :collaborations
  has_many :collaborating_sections, through: :collaborations, source: :section
  
  # favourites associations
  has_many :favourites
  has_many :favourite_sections, through: :favourites, source: :section

  before_save :set_full_name

  acts_as_voter

  # concatenated collection of posts and comments in descending order
  def contributions
    @contributions = posts + comments + sections + favourites

    @contributions.sort_by(&:created_at).reverse
  end

  def next_favourite_position
    @current_position = self.favourites.maximum('position')

    if @current_position.blank?
      @current_position = 0
    end

    @current_position += 1

    @current_position
  end

  # loop through favourites and change position if neccesary
  def consolidate_favourite_positions
    @last_position = 0

    self.favourites.each do |favourite|
      if (favourite.position - @last_position) > 1
        # set new position to be directly after the last favourite
        @new_position = @last_position + 1

        # change position of favourite
        favourite.position = @new_position
        favourite.save

        @last_position = @new_position
      else
        @last_position = favourite.position
      end
    end
  end

  private
    # concatenate first and last names then set as full name
    def set_full_name
      self.full_name = [first_name, last_name].join(' ')
    end
end