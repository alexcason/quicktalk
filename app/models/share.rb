class Share
  include ActiveModel::Model

  attr_accessor :url, :message, :sender_id, :recipients

  validates_presence_of :url, :sender_id, :recipients
end