class Voteable < ActiveRecord::Base
  self.abstract_class = true

  acts_as_voteable

  SECONDS_TO_HOURS = 3600
  HOURS_ELAPSED_BOOSTER = 2
  GRAVITY = 1.8

  # calculate and update score using defined algorithm
  def update_score
    hours_since_creation = (Time.now - created_at) / SECONDS_TO_HOURS

    score = plusminus / (hours_since_creation + HOURS_ELAPSED_BOOSTER) ** GRAVITY

    update_column(:score, score)
  end

  def self.order_by_score
    order('score DESC')
  end
end