class Post < Voteable
  belongs_to :user
  belongs_to :section

  has_many :comments, dependent: :destroy

  validates :title, :user_id, :section_id, presence: true
  validate :link_url_or_content_must_be_set

  self.per_page = 20

  def short_link_url
    URI.parse(self.link_url).host
  end

  # checks if passed in user is post owner or section owner/collaborator
  def is_action_authorised(user)
    self.user == user || section.is_owner(user)
  end

  private

    def link_url_or_content_must_be_set
      if link_url.blank? and content.blank?
        errors[:base] << 'One or both of the link_url and content fields must be set'
      end
    end
end