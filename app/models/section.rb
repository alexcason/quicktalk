class Section < ActiveRecord::Base
  belongs_to :user
  has_many :posts, dependent: :destroy

  # collaborators associations
  has_many :collaborations
  has_many :collaborators, through: :collaborations, source: :user

  # favourites associations
  has_many :favourites
  has_many :favouriting_users, through: :favourites, source: :user

  # last section visit association
  has_many :last_section_visits

  validates :title, :user_id, presence: true
  validates :title, uniqueness: true

  after_create :add_owner_favourite

  def unread_post_count(user)
    @post_count = 0

    @last_user_visit = self.last_section_visits.find_by_user_id(user.id)

    if @last_user_visit
      @post_count = self.posts.where('created_at > ? and user_id != ?', @last_user_visit.last_visit_at, user.id).length
    end

    @post_count
  end

  def set_last_user_visit(user)
    @last_user_visit = self.last_section_visits.where(user_id: user.id).first_or_initialize
    @last_user_visit.last_visit_at = Time.now
    @last_user_visit.save
  end

  # overide find_by_title to be case insensitive
  def self.find_by_title(title)
    Section.where('lower(title) =?', title.downcase).first
  end

  # overide to_param to provide title instead of id
  def to_param
  	self.title.downcase
  end

  def is_user_favourite(user)
    self.favouriting_users.include?(user)
  end

  # checks if passed in user is owner
  def is_owner(user)
    self.user == user
  end

  # checks if passed in user is owner or collaborator
  def is_owner_or_collaborator(user)
    is_owner(user) || self.collaborators.include?(user)
  end

  def is_view_authorised(user)
    !private or (private and is_owner_or_collaborator(user))
  end

  # gets sections that user has authorisation to view
  def self.all_authorised(user)
    # public section or private section and user is owner or collaborator
    Section.find_by_sql(["SELECT sections.id, sections.title, sections.user_id, sections.created_at FROM sections
      LEFT JOIN collaborations ON collaborations.section_id = sections.id AND collaborations.user_id = ?
      WHERE sections.private = false OR (sections.private = true AND (sections.user_id = ? OR collaborations.id IS NOT NULL))", user.id, user.id])
  end

  private
    def add_owner_favourite
      self.favourites.build(section: self, user: self.user)

      self.save
    end
end