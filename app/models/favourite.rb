class Favourite < ActiveRecord::Base
  belongs_to :section
  belongs_to :user

  # set default scope to include section association and order by position
  default_scope includes(:section).order('favourites.position ASC')

  validates :section, :user, :position, presence: true
  validates :section, uniqueness: { scope: :user, message: 'favourite must be unique' }, :if => :section_present

  after_destroy :consolidate_user_favourite_positions

  def change_position(direction)
    @success = false
    @current_position = self.position
    @max_position = self.user.favourites.maximum('position')

    # add functionality to change favourite position - remember to update adjacent favourite as well
    if (direction == 'up' && @current_position > 1) || (direction == 'down' && @current_position < @max_position)
      # get adjacent favourite record based upon direction of change
      if direction == 'up'
        @adjacent_favourite = self.user.favourites.find_by_position(@current_position - 1)
      elsif direction == 'down'
        @adjacent_favourite = self.user.favourites.find_by_position(@current_position + 1)
      end

      # ensure that there is an adjacent favourite, no need to update if not
      if !@adjacent_favourite.nil?
        self.position = @adjacent_favourite.position
        @adjacent_favourite.position = @current_position

        # save position changes as one transaction
        Favourite.transaction do
          @success = self.save
          @success = @adjacent_favourite.save && @success
        end
      end
    end

    @success
  end

  private
    def section_present
      section.present?
    end

    def consolidate_user_favourite_positions
      # asynchronously consolidate user favourite positions
      self.user.delay.consolidate_favourite_positions
    end
end