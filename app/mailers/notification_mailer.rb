class NotificationMailer < ActionMailer::Base
  default from: 'alex.cason@codingcoffeeclub.com'

  def share_email(share)
    # retrieve common details
    @url = share.url
    @message = share.message
    @sender = User.find(share.sender_id)
    @subject = @sender.full_name + ' Has Shared a Quicktalk Page With You'
    
    # loop through recipients and send mail to each
    share.recipients.each do |r|
      @recipient = User.find(r)

      mail(to: @recipient.email, subject: @subject)
    end
  end
end