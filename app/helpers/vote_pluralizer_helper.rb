# takes a number and a word and outputs a phrase which is pluralized if not equal to one

module VotePluralizerHelper
	VOTE_TEXT = 'vote'
  
  def vote_pluralizer(voteable)
    vote_count = voteable.plusminus
  	text = vote_count.to_s + ' ' + VOTE_TEXT

    if vote_count != 1
      text << 's'
    end

    text
  end

end