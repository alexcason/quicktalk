module PluralizerHelper
  def pluralizer(count, text)
    if count != 1
      text << 's'
    else
      text
    end
  end
end