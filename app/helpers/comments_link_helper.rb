# takes a collection of comments and a path and outputs a link to the post show action which changes depending upon if someone has already commented

module CommentsLinkHelper

  COMMENTS_TEXT = 'discuss'
  COMMENTS_TEXT_PLURAL = 'comments'

  def comments_link(comments, path)
    if comments.size > 0
      text = comments.size.to_s + ' ' + COMMENTS_TEXT_PLURAL
    else    
      text = COMMENTS_TEXT
    end

    link_to(text, path)
  end

end