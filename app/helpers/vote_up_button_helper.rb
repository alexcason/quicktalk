# takes a path and returns a vote up interface

module VoteUpButtonHelper
  VOTE_UP_TEXT = 'Vote Up'

  def vote_up_button(path)
    button_to('', path, form_class: 'qt-form-vote', title: VOTE_UP_TEXT)
  end
end