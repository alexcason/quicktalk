quicktalk.sections = {
  init: function() {
    $('a.share-link').click(function () {
      $(this).toggleClass('selected');

      $('#share-link-interface').toggle();

      $('#share-link-interface .selectize-input input').focus();

      return false;
    });

    $(document).mouseup(function (e) {
      var shareLink = $('a.share-link');
      var shareLinkInterface = $('#share-link-interface');

      // if click was not on share link, share link interface or descendents
      if (!shareLink.is(e.target) && shareLink.has(e.target).length === 0 && !shareLinkInterface.is(e.target) && shareLinkInterface.has(e.target).length === 0) {
        // de-select share link
        $('a.share-link').removeClass('selected');

        // hide share link interface
        shareLinkInterface.hide();
      }
    });
  }
};

$(document).ready(quicktalk.sections.init);
document.addEventListener('page:load', quicktalk.sections.init);