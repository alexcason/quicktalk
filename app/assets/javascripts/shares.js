quicktalk.shares = {
  initSelect: function() {
    $('#share_recipients').selectize({
      create: false,
      valueField: 'id',
      labelField: 'full_name',
      searchField: ['full_name'],
      render: {
        item: function(item, escape) {
          return '<div>' +
            (item.full_name ? '<span class="name">' + escape(item.full_name) + '</span>' : '') +
            (item.email ? '<span class="email">' + escape(item.email) + '</span>' : '') +
          '</div>';
        },
        option: function(item, escape) {
          var label = item.full_name || item.email;
          var caption = item.full_name ? item.email : null;
          return '<div>' +
            '<span class="label">' + escape(label) + '</span>' +
            (caption ? '<span class="caption">' + escape(caption) + '</span>' : '') +
          '</div>';
        }
      },
      load: function(query, callback) {
        if (!query.length) return callback();
        $.ajax({
            url: '/search',
            type: 'GET',
            dataType: 'json',
            data: {
              t: 'user',
              q: query,
              page_limit: 10,
              transform: 'share_recipients'
            },
            error: function() {
                callback();
            },
            success: function(res) {
                callback(res);
            }
        });
      }
    });
  }
};

$(document).ready(quicktalk.shares.initSelect);
document.addEventListener('page:load', quicktalk.shares.initSelect);