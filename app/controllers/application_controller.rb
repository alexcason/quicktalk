class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authenticate_user!
  before_action :set_authorised_sections

  def set_authorised_sections
    if user_signed_in?
  	 @menu_sections = Section.all_authorised(current_user)
     @menu_favourites = current_user.favourites
    end
  end
end
