module VoteableConcern
  extend ActiveSupport::Concern

  included do
    before_action :validate_voted_on!, only: [:vote_up]
    after_action :update_scores, only: [:vote_up]
  end
end