class SearchController < ApplicationController
  # GET /search
  # GET /search.json
  def index
    # filter results by type
    case params[:t]
      when 'users'
        @results = User.all
      when 'posts'
        @results = Post.all
      when 'sections'
        @results = Section.all
      else
        @results = User.all
    end

    # limit results to page_limit count
    page_limit = params[:page_limit]

    if page_limit.present? && page_limit.is_i?
      # convert page_limit to integer
      page_limit = Integer(page_limit)

      if page_limit > 0
        @results = @results.take(page_limit)
      end
    end

    # transform results by format
    case params[:transform]
      when 'share_recipients'
        @results = @results.to_json(:only => [ :id, :full_name, :email ])
      else
        # no format specified - do nothing
    end

    respond_to do |format|
      format.json { render json: @results }
    end
  end
end
