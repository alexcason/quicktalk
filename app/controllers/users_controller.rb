class UsersController < ApplicationController
  # user controller shares path with devise, limit actions to user controller actions only
  before_action :set_user, only: [:show, :update]
  before_action :authorise_current_user!, only: [:update]

  # GET /users/1
  # GET /users/1.json
  def show
    case params[:tab]
    when 'posts'
      # Posts
      @contributions = @user.posts.includes(:user, :section, :comments, :votes).order('created_at DESC').paginate(:page => params[:page])
    when 'comments'
      # Comments
      @contributions = @user.comments.includes(:user, :post, :votes).order('created_at DESC').paginate(:page => params[:page])
    when 'sections'
      # Sections
      @contributions = @user.sections.includes(:user, :posts).order('created_at DESC').paginate(:page => params[:page])
    when 'favourites'
      #Favourites
      @contributions = @user.favourites.includes(:section).order('created_at DESC').paginate(:page => params[:page])
    else
      # Everything
      @contributions = @user.contributions.paginate(:page => params[:page])
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to profile_settings_path, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def set_user
      @user = User.find(params[:id])
    end

    def authorise_current_user!
      if @user != current_user
        redirect_to user_path(@user)
      end
    end

    def user_params
      params.require(:user).permit(:first_name, :last_name, :email, :phone, :department, :location, :company)
    end
end