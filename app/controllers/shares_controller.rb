class SharesController < ApplicationController
  # GET /share/new
  # GET /share/new.json
  def new
    @share = Share.new
  end

  # POST /share
  # POST /share.json
  def create
    @share = Share.new(share_params)
    @share.url = request.original_url
    @share.sender_id = current_user.id
    
    respond_to do |format|
      if @share.valid?
        NotificationMailer.share_email(@share)

        format.html { redirect_to new_share_path, notice: 'URL was successfully shared.' }
        # format.json { render action: 'show', status: :created, location: section_post_path(@section, @post) }
      else
        format.html { render action: 'new' }
        format.json { render json: @share.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def share_params
      params.require(:share).permit(:message, :recipients => [])
    end
end