class FavouritesController < ApplicationController
  before_action :set_section
  before_action :set_favourite, only: [:update, :destroy]
  before_action :authorise_user!
  before_action :authorise_owner!, only: [:destroy]

  # POST /favourites
  # POST /favourites.json
  def create
    @section.favourites.build(section: @section, user: current_user, position: current_user.next_favourite_position)

    respond_to do |format|
      if @section.save
        format.html { redirect_to :back, notice: 'Favourite was successfully added.' }
        format.json { render action: 'index', status: :created, location: section_path(@section) }
      else
        format.html { redirect_to root_path, notice: 'Favourite was not added successfully.' }
        format.json { render json: @section.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /favourites/1
  # DELETE /favourites/1.json
  def destroy
    @favourite.destroy

    respond_to do |format|
      format.html { redirect_to :back }
      format.json { head :no_content }
    end
  end

  # PATCH /favourites/1
  # PATCH /favourites/1.json
  def update
    @direction = params[:favourite][:direction]

    respond_to do |format|
      if @favourite.change_position(@direction)
        format.html { redirect_to :back, notice: 'Favourite was successfully updated.' }
        format.json { render action: 'favourites', status: :updated, location: :back }
      else
        format.html { redirect_to :back, notice: 'Favourite was not updated successfully.' }
        format.json { render json: @favourite.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def set_section
      @section = Section.find_by_title(params[:section_id])
    end

    def set_favourite
      @favourite = Favourite.find(params[:id])
    end

    def favourite_params
      params.require(:favourite).permit(:direction)
    end

    # TODO: refactor into common code
    def authorise_user!
      if !@section.is_view_authorised(current_user)
        redirect_to root_path
      end
    end

    def authorise_owner!
      if @favourite.user != current_user
        redirect_to section_path(@section)
      end
    end
end
