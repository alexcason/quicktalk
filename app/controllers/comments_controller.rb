class CommentsController < ApplicationController
  before_action :set_section
  before_action :set_post
  before_action :set_comment, only: [:vote_up]
  before_action :authorise_user!

  include VoteableConcern
  
  # POST /comments
  # POST /comments.json
  def create
  	@comment = @post.comments.build(comment_params)
  	@comment.user = current_user

  	respond_to do |format|
  	  if @post.save!
        format.html { redirect_to section_post_path(@section, @post), notice: 'Comment was successfully created.' }
        # TODO: fix json render action to point at posts show action
        format.json { render action: 'show', status: :created, location: section_post_path(@section, @post) }
  	  else
        format.html { redirect_to section_post_path(@section, @post) }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
  	  end
  	end
  end

  # POST /comments/1/vote_up
  def vote_up
    current_user.vote_for(@comment)

    redirect_to section_post_path(@section, @post), notice: 'Your vote has been successfully added.'
  end

  private
    def set_comment
      @comment = Comment.find(params[:comment_id])
    end

    def set_post
      @post = Post.find(params[:post_id])
    end

    def set_section
      @section = Section.find_by_title(params[:section_id])
    end

    # TODO: refactor into common code
    def authorise_user!
      if !@section.is_view_authorised(current_user)
        redirect_to root_path
      end
    end

    def validate_voted_on!
      if @comment.user == current_user
        redirect_to section_post_path(@section, @post), notice: 'You can\'t vote on your own comment.'
      elsif current_user.voted_on?(@comment)
        redirect_to section_post_path(@section, @post), notice: 'You have already voted on this comment.'
      end
    end

    def update_scores
      Delayed::Job.enqueue VoteableCommentsJob.new(@post)
    end

    def comment_params
      params.require(:comment).permit(:content)
    end
end