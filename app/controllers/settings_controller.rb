class SettingsController < ApplicationController
  # GET /settings/profile
  # GET /settings/profile.json
  def profile
  end

  # GET /settings/favourites
  # GET /settings/favourites.json
  def favourites
    @favourites = current_user.favourites
  end
end