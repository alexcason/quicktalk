class SectionsController < ApplicationController
  before_action :set_section, only: [:show, :edit, :update, :destroy]
  before_action :set_favourite, only: [:show, :edit]
  before_action :authorise_user!, only: [:show, :edit, :update, :destroy]
  before_action :authorise_owner!, only: [:edit, :update, :destroy]

  # GET /sections
  # GET /sections.json
  def index
    @sections = Section.all_authorised(current_user).paginate(:page => params[:page])
  end

  # GET /sections/1
  # GET /sections/1.json
  def show
    # order by score and paginate results
    @posts = @section.posts.includes(:section, :user).order_by_score.paginate(:page => params[:page])

    @counter_reset = (params[:page].to_i > 1 ? params[:page].to_i * 10 : 0)

    # record user visit to section
    @section.set_last_user_visit(current_user)
  end

  # GET /sections/new
  def new
    @section = Section.new
  end

  # GET /sections/1/edit
  def edit
  end

  # POST /sections
  # POST /sections.json
  def create
    @section = current_user.sections.build(section_params)

    respond_to do |format|
      if @section.save
        format.html { redirect_to section_path(@section), notice: 'Section was successfully created.' }
        format.json { render action: 'show', status: :created, location: section_path(@section) }
      else
        format.html { render action: 'new' }
        format.json { render json: @section.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sections/1
  # PATCH/PUT /sections/1.json
  def update
    respond_to do |format|
      if @section.update(section_params)
        format.html { redirect_to edit_section_path(@section), notice: 'Section was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @section.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sections/1
  # DELETE /sections/1.json
  def destroy
    @section.destroy
    respond_to do |format|
      format.html { redirect_to root_path }
      format.json { head :no_content }
    end
  end

  private
    def set_section
      @section = Section.find_by_title(params[:id])
    end

    # TODO: refactor into common code
    def set_favourite
      @favourite = @section.favourites.where(user_id: current_user.id).first
    end

    def section_params
      params.require(:section).permit(:user_id, :title, :description, :private)
    end

    # TODO: refactor into common code
    def authorise_user!
      if !@section.is_view_authorised(current_user)
        redirect_to root_path
      end
    end

    def authorise_owner!
      if !@section.is_owner(current_user)
        redirect_to section_path(@section)
      end
    end
end
