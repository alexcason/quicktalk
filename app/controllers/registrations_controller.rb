class RegistrationsController < Devise::RegistrationsController
  before_filter :configure_permitted_parameters

  before_action :check_secret, only: [:create]

  protected
    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_up) {|u| u.permit(:first_name, :last_name, :email, :password, :password_confirmation)}
    end

    # override redirect after user update
    def after_update_path_for(resource)
      user_path(resource)
    end

    # TODO: remove secret check
    def check_secret
      @sign_up_secret = params[:secret]

      if @sign_up_secret.nil? || @sign_up_secret != 'nero'
        redirect_to new_user_registration_path
      end
    end
end