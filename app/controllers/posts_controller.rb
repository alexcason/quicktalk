class PostsController < ApplicationController
  before_action :set_section, except: [:index_all]
  before_action :set_post, only: [:show, :edit, :update, :destroy, :vote_up, :vote_down]
  before_action :set_favourite, only: [:index, :show, :new, :edit, :update]
  before_action :authorise_user!, except: [:index_all]
  before_action :authorise_owner!, only: [:edit, :update, :destroy]
  
  # must occur after section and post have been set
  include VoteableConcern

  # GET /posts
  # GET /posts.json
  def index_all
    # order by score and paginate results
    @posts = Post.includes(:section, :user).order_by_score.paginate(:page => params[:page])

    @counter_reset = (params[:page].to_i > 1 ? params[:page].to_i * 10 : 0)
  end

  # GET /posts
  # GET /posts.json
  def index
    # order by score and paginate results
    @posts = @section.posts.order_by_score.paginate(:page => params[:page])

    @counter_reset = (params[:page].to_i > 1 ? params[:page].to_i * 10 : 0)
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    @comment = Comment.new

    @post_comments = @post.comments.order_by_score
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = @section.posts.build(post_params)
    @post.user = current_user

    respond_to do |format|
      if @post.save
        format.html { redirect_to section_post_path(@section, @post), notice: 'Post was successfully created.' }
        format.json { render action: 'show', status: :created, location: section_post_path(@section, @post) }
      else
        format.html { render action: 'new' }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to section_post_path(@section, @post), notice: 'Post was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to section_path(@section) }
      format.json { head :no_content }
    end
  end

  # POST /posts/1/vote_up
  def vote_up
    current_user.vote_for(@post)

    redirect_to section_post_path(@section, @post), notice: 'Your vote has been successfully added.'
  end

  private
    def set_section
      @section = Section.find_by_title(params[:section_id])
    end

    def set_post
      @post = Post.find(params[:id])
    end

    # TODO: refactor into common code
    def set_favourite
      @favourite = @section.favourites.where(user_id: current_user.id).first
    end

    # TODO: refactor into common code
    def authorise_user!
      if !@section.is_view_authorised(current_user)
        redirect_to root_path
      end
    end

    def authorise_owner!
      if !@post.is_action_authorised(current_user)
        redirect_to section_post_path(@section, @post), notice: 'You do not have permission to update this post.'
      end
    end

    def validate_voted_on!
      if @post.user == current_user
        redirect_to section_post_path(@section, @post), notice: 'You can\'t vote on your own post.'
      elsif current_user.voted_on?(@post)
        redirect_to section_post_path(@section, @post), notice: 'You have already voted on this post.'
      end
    end

    def update_scores
      Delayed::Job.enqueue VoteablePostsJob.new()
    end

    def post_params
      params.require(:post).permit(:title, :link_url, :content)
    end
end
