class CollaboratorsController < ApplicationController
  before_action :set_section
  before_action :set_collaboration, only: [:destroy]
  before_action :authorise_user!
  before_action :authorise_owner!

  # POST /collaborators
  # POST /collaborators.json
  def create
    @section.collaborations.build(collaboration_params)

    respond_to do |format|
      if @section.save
        format.html { redirect_to edit_section_path(@section), notice: 'Collaborator was successfully created.' }
        format.json { render action: 'edit', status: :created, location: edit_section_path(@section) }
      else
        format.html { redirect_to edit_section_path(@section) }
        format.json { render json: @section.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /collaborators/1
  # DELETE /collaborators/1.json
  def destroy
    @collaboration.destroy
    
    respond_to do |format|
      format.html { redirect_to edit_section_path(@section) }
      format.json { head :no_content }
    end
  end

  private
    def set_section
      @section = Section.find_by_title(params[:section_id])
    end

    def set_collaboration
      @collaboration = Collaboration.find(params[:id])
    end

    # TODO: refactor into common code
    def authorise_user!
      if !@section.is_view_authorised(current_user)
        redirect_to root_path
      end
    end

    def authorise_owner!
      if !@section.is_owner(current_user)
        redirect_to section_path(@section)
      end
    end

    def collaboration_params
      params.require(:collaboration).permit(:user_id)
    end
end
