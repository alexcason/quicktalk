require 'test_helper'

class CollaborationTest < ActiveSupport::TestCase
  setup do
    @regular_user_one = users(:regular_user_one)
    @private_section = sections(:private_section)
    @private_section_collaboration_with_section_collaborator_one = collaborations(:private_section_collaboration_with_section_collaborator_one)
  end

  test 'collaborations must have a section' do
    collaboration = Collaboration.new(user: @regular_user_one)

    assert collaboration.invalid?, 'Saved a collaboration without a section'
  end

  test 'collaborations must have a user' do
    collaboration = Collaboration.new(section: @private_section)

    assert collaboration.invalid?, 'Saved a collaboration with a user'
  end

  test 'collaboration users must not be section owner' do
    collaboration = Collaboration.new(section: @private_section,
                                      user: @private_section.user)

    assert collaboration.invalid?, 'Saved a collaboration with a user who is already section owner'
  end

  test 'collaborations must be unique' do
    collaboration = Collaboration.new(section: @private_section_collaboration_with_section_collaborator_one.section,
                                      user: @private_section_collaboration_with_section_collaborator_one.user)

    assert collaboration.invalid?, 'Saved a duplicate collaboration'
  end
end