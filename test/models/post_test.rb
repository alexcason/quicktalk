require 'test_helper'

class PostTest < ActiveSupport::TestCase
  setup do
    @regular_user_one = users(:regular_user_one)
    @public_section = sections(:public_section)
  end

  test 'posts must have a title' do
    post = Post.new(content: 'Post with no title.',
                    user: @regular_user_one,
                    section: @public_section)

    assert post.invalid?, 'Saved a post without a title'
  end

  test 'posts must have a user' do
    post = Post.new(title: 'Post with No User',
                    content: 'Test content.',
                    section: @public_section)

    assert post.invalid?, 'Saved a post without a user'
  end

  test 'posts must have a section' do
    post = Post.new(title: 'Post with No Section',
                    content: 'Test content.',
                    user: @regular_user_one)

    assert post.invalid?, 'Saved a post without a section'
  end

  test 'posts must have a link or content' do
    post = Post.new(title: 'Post with No Link or Content',
                    user: @regular_user_one,
                    section: @public_section)

    assert post.invalid?, 'Saved a post without link_url or content'
  end
end
