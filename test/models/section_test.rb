require 'test_helper'

class SectionTest < ActiveSupport::TestCase
  setup do
    @section_owner = users(:section_owner)
    @public_section = sections(:public_section)
  end

  test 'sections must have a title' do
    section = Section.new(user: @section_owner)

    assert section.invalid?, 'Saved a section without a title'
  end

  test 'sections must have a user' do
    section = Section.new(title: 'Section with No User')

    assert section.invalid?, 'Saved a section without a user'
  end

  test 'section titles must be unique' do
    section = Section.new(title: @public_section.title,
                          user: @section_owner)

    assert section.invalid?, 'Saved a section with a duplicate title'
  end

  test 'section should be added as user favourite after creation' do
    section = Section.new(title: 'Section Favourite Test',
                          user: @section_owner)

    # save section to run after_create callback
    section.save

    assert section.favouriting_users.exists?(@section_owner), 'Saved a section without adding as user favourite after creation'
  end
end
