require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  setup do
    @regular_user_two = users(:regular_user_two)
    @public_section_post_by_regular_user_one = posts(:public_section_post_by_regular_user_one)
  end

  test 'comments must have content' do
    comment = Comment.new(user: @regular_user_two,
                          post: @public_section_post_by_regular_user_one)

    assert comment.invalid?, 'Saved a comment without content'
  end

  test 'comments must have a user' do
    comment = Comment.new(content: 'Comment with no user.',
                          post: @public_section_post_by_regular_user_one)

    assert comment.invalid?, 'Saved a comment without a user'
  end

  test 'comments must have a post' do
    comment = Comment.new(content: 'Comment with no post.',
                          user: @regular_user_two)

    assert comment.invalid?, 'Saved a comment without a post'
  end
end
