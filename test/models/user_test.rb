require 'test_helper'

class UserTest < ActiveSupport::TestCase
  setup do
    @section_owner = users(:section_owner)
    @regular_user_one = users(:regular_user_one)
  end

  test 'user emails must be unique' do
    user = User.create(email: @regular_user_one.email, first_name: @regular_user_one.first_name, last_name: @regular_user_one.last_name, password: 'password', password_confirmation: 'password')
  
    assert user.invalid?, 'Saved a user with a duplicate email'
  end

  test 'user contributions must contain posts, comments and sections' do
    post_count = 0
    comment_count = 0
    section_count = 0

    @section_owner.contributions.each do |contribution|
      if contribution.class == Post
        post_count = post_count + 1
      elsif contribution.class == Comment
        comment_count = comment_count + 1
      elsif contribution.class == Section
        section_count = section_count + 1
      end
    end

    assert post_count > 0, 'Contributions do not contain posts'
    assert comment_count > 0, 'Contributions do not contain comments'
    assert section_count > 0, 'Contributions do not contain sections'
  end
end
