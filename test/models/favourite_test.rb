require 'test_helper'

class FavouriteTest < ActiveSupport::TestCase
  setup do
    @regular_user_one = users(:regular_user_one)
    @public_section = sections(:public_section)

    @public_section_favourite_for_regular_user_two = favourites(:public_section_favourite_for_regular_user_two)
  end

  test 'favourites must have a section' do
    favourite = Favourite.new(user: @regular_user_one)

    assert favourite.invalid?, 'Saved a favourite without a section'
  end

  test 'favourites must have a user' do
    favourite = Favourite.new(section: @public_section)

    assert favourite.invalid?, 'Saved a favourite without a user'
  end

  test 'favourites must have a position' do
    favourite = Favourite.new(section: @public_section,
                              user: @regular_user_one)

    assert favourite.invalid?, 'Saved a favourite without a position'
  end

  test 'favourites must be unique' do
    favourite = Favourite.new(section: @public_section_favourite_for_regular_user_two.section,
                              user: @public_section_favourite_for_regular_user_two.user)

    assert favourite.invalid?, 'Saved a duplicate favourite'
  end
end
