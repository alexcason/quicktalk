require 'test_helper'

class CollaboratorsControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  setup do
    @section_owner = users(:section_owner)
    @regular_user_one = users(:regular_user_one)
    @regular_user_two = users(:regular_user_two)
    @section_collaborator_one = users(:section_collaborator_one)
    
    @private_section = sections(:private_section)
    @private_section_collaboration_with_section_collaborator_one = collaborations(:private_section_collaboration_with_section_collaborator_one)
  end

  test 'section owner should create collaboration' do
    sign_in @section_owner

    assert_difference('Collaboration.count') do
      post :create, section_id: @private_section.title, collaboration: { user_id: @regular_user_one }
    end

    assert_redirected_to edit_section_path(@private_section)
  end

  test 'section owner should destroy collaboration' do
    sign_in @section_owner

    assert_difference('Collaboration.count', -1) do
      delete :destroy, section_id: @private_section.title, id: @private_section_collaboration_with_section_collaborator_one.id

      assert_redirected_to edit_section_path(@private_section)
    end
  end

  test 'section collaborator should not create collaboration' do
    sign_in @section_collaborator_one

    assert_no_difference('Collaboration.count') do
      post :create, section_id: @private_section.title, collaboration: { user_id: @regular_user_one }
    end
  end

  test 'section collaborator should not destroy collaboration' do
    sign_in @section_collaborator_one

    assert_no_difference('Collaboration.count') do
      delete :destroy, section_id: @private_section.title, id: @private_section_collaboration_with_section_collaborator_one.id
    end
  end

  test 'regular user should not create collaboration' do
    sign_in @regular_user_two

    assert_no_difference('Collaboration.count') do
      post :create, section_id: @private_section.title, collaboration: { user_id: @regular_user_one }
    end
  end

  test 'regular user should not destroy collaboration' do
    sign_in @regular_user_two

    assert_no_difference('Collaboration.count') do
      delete :destroy, section_id: @private_section.title, id: @private_section_collaboration_with_section_collaborator_one.id
    end
  end
end