require 'test_helper'

class FavouritesControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  setup do
    @public_section = sections(:public_section)
    @private_section = sections(:private_section)

    @section_owner = users(:section_owner)
    @section_collaborator_one = users(:section_collaborator_one)
    @section_collaborator_two = users(:section_collaborator_two)
    @regular_user_one = users(:regular_user_one)
    @regular_user_two = users(:regular_user_two)

    @public_section_favourite_for_section_collaborator_one = favourites(:public_section_favourite_for_section_collaborator_one)
    @public_section_favourite_for_regular_user_two = favourites(:public_section_favourite_for_regular_user_two)
    @private_section_favourite_for_section_collaborator_one = favourites(:private_section_favourite_for_section_collaborator_one)
    @private_section_favourite_for_regular_user_two = favourites(:private_section_favourite_for_regular_user_two)
  end

  # public section

  test 'public - section owner should create favourite' do
    sign_in @section_owner

    assert_difference('Favourite.count') do
      post :create, section_id: @public_section
    end

    assert_redirected_to section_path(@public_section)
  end

  test 'public - section owner should destroy favourite if owner' do
    sign_in @section_owner

    public_section_favourite_for_section_owner = Favourite.create(section: @public_section,
                                                                  user: @section_owner)

    assert_difference('Favourite.count', -1) do
      delete :destroy, section_id: @public_section, id: public_section_favourite_for_section_owner
    end

    assert_redirected_to section_path(@public_section)
  end

  test 'public - section owner should not destroy favourite if not owner' do
    sign_in @section_owner

    assert_no_difference('Favourite.count') do
      delete :destroy, section_id: @public_section, id: @public_section_favourite_for_section_collaborator_one
    end

    assert_redirected_to section_path(@public_section)
  end

  test 'public - section collaborator should create favourite' do
    sign_in @section_collaborator_two

    assert_difference('Favourite.count') do
      post :create, section_id: @public_section
    end

    assert_redirected_to section_path(@public_section)
  end

  test 'public - section collaborator should destroy favourite if owner' do
    sign_in @section_collaborator_one

    assert_difference('Favourite.count', -1) do
      delete :destroy, section_id: @public_section, id: @public_section_favourite_for_section_collaborator_one
    end

    assert_redirected_to section_path(@public_section)
  end

  test 'public - section collaborator should not destroy favourite if not owner' do
    sign_in @section_collaborator_one

    assert_no_difference('Favourite.count') do
      delete :destroy, section_id: @public_section, id: @public_section_favourite_for_regular_user_two
    end

    assert_redirected_to section_path(@public_section)
  end

  test 'public - regular user should create favourite' do
    sign_in @regular_user_one

    assert_difference('Favourite.count') do
      post :create, section_id: @public_section
    end

    assert_redirected_to section_path(@public_section)
  end

  test 'public - regular user should destroy favourite if owner' do
    sign_in @regular_user_two

    assert_difference('Favourite.count', -1) do
      delete :destroy, section_id: @public_section, id: @public_section_favourite_for_regular_user_two
    end

    assert_redirected_to section_path(@public_section)
  end

  test 'public - regular user should not destroy favourite if not owner' do
    sign_in @regular_user_one

    assert_no_difference('Favourite.count') do
      delete :destroy, section_id: @public_section, id: @public_section_favourite_for_regular_user_two
    end

    assert_redirected_to section_path(@public_section)
  end

  # private section

  test 'private - section owner should create favourite' do
    sign_in @section_owner

    assert_difference('Favourite.count') do
      post :create, section_id: @private_section
    end

    assert_redirected_to section_path(@private_section)
  end

  test 'private - section owner should destroy favourite if owner' do
    sign_in @section_owner

    private_section_favourite_for_section_owner = Favourite.create(section: @private_section,
                                                                   user: @section_owner)

    assert_difference('Favourite.count', -1) do
      delete :destroy, section_id: @private_section, id: private_section_favourite_for_section_owner
    end

    assert_redirected_to section_path(@private_section)
  end

  test 'private - section owner should not destroy favourite if not owner' do
    sign_in @section_owner

    assert_no_difference('Favourite.count') do
      delete :destroy, section_id: @private_section, id: @private_section_favourite_for_regular_user_two
    end

    assert_redirected_to section_path(@private_section)
  end

  test 'private - section collaborator should create favourite' do
    sign_in @section_collaborator_two

    assert_difference('Favourite.count') do
      post :create, section_id: @private_section
    end

    assert_redirected_to section_path(@private_section)
  end

  test 'private - section collaborator should destroy favourite if owner' do
    sign_in @section_collaborator_one

    assert_difference('Favourite.count', -1) do
      delete :destroy, section_id: @private_section, id: @private_section_favourite_for_section_collaborator_one
    end

    assert_redirected_to section_path(@private_section)
  end

  test 'private - section collaborator should not destroy favourite if not owner' do
    sign_in @section_collaborator_one

    assert_no_difference('Favourite.count') do
      delete :destroy, section_id: @private_section, id: @private_section_favourite_for_regular_user_two
    end

    assert_redirected_to section_path(@private_section)
  end

  test 'private - regular user should not create favourite' do
    sign_in @regular_user_one

    assert_no_difference('Favourite.count') do
      post :create, section_id: @private_section
    end

    assert_redirected_to root_path
  end

  test 'private - regular user should not destroy favourite if owner' do
    sign_in @regular_user_two

    assert_no_difference('Favourite.count') do
      delete :destroy, section_id: @private_section, id: @private_section_favourite_for_regular_user_two
    end

    assert_redirected_to root_path
  end

  test 'private - regular user should not destroy favourite if not owner' do
    sign_in @regular_user_one

    assert_no_difference('Favourite.count') do
      delete :destroy, section_id: @private_section, id: @private_section_favourite_for_regular_user_two
    end

    assert_redirected_to root_path
  end

  # regular user should not get existing favourite in top menu if private
end
