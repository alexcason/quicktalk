require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  setup do
    @regular_user_one = users(:regular_user_one)
    @regular_user_two = users(:regular_user_two)
  end

  test 'should show user' do
    sign_in @regular_user_two

    get :show, id: @regular_user_one

    assert_response :success
  end

  test 'should update user if current user' do
    sign_in @regular_user_one

    test_first_name = 'Test'
    test_last_name = 'User'
    test_email = 'test@user.co.uk'
    test_phone = '01234567891'
    test_department = 'Test Department'
    test_location = 'Test Location'
    test_company = 'Test Company'

    patch :update, id: @regular_user_one, user: { first_name: test_first_name, last_name: test_last_name, email: test_email, phone: test_phone, department: test_department, location: test_location, company: test_company }

    # ensure properties have been changed
    assert_equal test_first_name, assigns(:user).first_name
    assert_equal test_last_name, assigns(:user).last_name
    assert_equal test_email, assigns(:user).email
    assert_equal test_phone, assigns(:user).phone
    assert_equal test_department, assigns(:user).department
    assert_equal test_location, assigns(:user).location
    assert_equal test_company, assigns(:user).company

    assert_redirected_to profile_settings_path
  end

  test 'should not update user if not current user' do
    sign_in @regular_user_two

    patch :update, id: @regular_user_one, user: { first_name: 'Test', last_name: 'User', email: 'test@user.co.uk', phone: '01234567891', department: 'Test Department', company: 'Test Company', location: 'Test Location' }

    # ensure properties have been changed
    assert_equal @regular_user_one.first_name, assigns(:user).first_name
    assert_equal @regular_user_one.last_name, assigns(:user).last_name
    assert_equal @regular_user_one.email, assigns(:user).email
    assert_equal @regular_user_one.phone, assigns(:user).phone
    assert_equal @regular_user_one.department, assigns(:user).department
    assert_equal @regular_user_one.location, assigns(:user).location
    assert_equal @regular_user_one.company, assigns(:user).company

    assert_redirected_to user_path(@regular_user_one)
  end
end