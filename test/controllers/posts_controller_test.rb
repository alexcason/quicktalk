require 'test_helper'

class PostsControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  setup do
    @section_owner = users(:section_owner)
    @section_collaborator_one = users(:section_collaborator_one)
    @regular_user_one = users(:regular_user_one)
    @regular_user_two = users(:regular_user_two)

    @public_section = sections(:public_section)
    @private_section = sections(:private_section)

    @public_section_post_by_section_owner = posts(:public_section_post_by_section_owner)
    @public_section_post_by_section_collaborator_one = posts(:public_section_post_by_section_collaborator_one)
    @public_section_post_by_regular_user_one = posts(:public_section_post_by_regular_user_one)
    @private_section_post_by_section_owner = posts(:private_section_post_by_section_owner)
    @private_section_post_by_section_collaborator_one = posts(:private_section_post_by_section_collaborator_one)
  end

  test 'all users should get index all' do
    sign_in @regular_user_one

    get :index_all

    assert_response :success
  end

  # public section
  test 'public - section owner should get new' do
    sign_in @section_owner

    get :new, section_id: @public_section

    assert_response :success
  end

  test 'public - section owner should create post' do
    sign_in @section_owner

    assert_difference('Post.count') do
      post :create, section_id: @public_section, post: { title: 'New Post Title', link_url: 'http://www.google.com/', content: 'New post content.' }
    end

    assert_redirected_to section_post_path(@public_section, assigns(:post))
  end

  test 'public - section owner should show post' do
    sign_in @section_owner

    get :show, section_id: @public_section, id: @public_section_post_by_regular_user_one

    assert_response :success
  end

  test 'public - section owner should get edit if owner' do
    sign_in @section_owner

    get :edit, section_id: @public_section, id: @public_section_post_by_section_owner

    assert_response :success
  end

  test 'public - section owner should get edit if not owner' do
    sign_in @section_owner

    get :edit, section_id: @public_section, id: @public_section_post_by_regular_user_one

    assert_response :success
  end

  test 'public - section owner should update post if owner' do
    sign_in @section_owner

    test_title = 'Updated Post Title'

    patch :update, section_id: @public_section, id: @public_section_post_by_section_owner, post: { title: test_title }

    assert_equal assigns(:post).title, test_title
    assert_redirected_to section_post_path(@public_section, assigns(:post))
  end

  test 'public - section owner should update post if not owner' do
    sign_in @section_owner

    test_title = 'Updated Post Title'

    patch :update, section_id: @public_section, id: @public_section_post_by_regular_user_one, post: { title: test_title }

    assert_equal assigns(:post).title, test_title
    assert_redirected_to section_post_path(@public_section, assigns(:post))
  end

  test 'public - section owner should destroy post if owner' do
    sign_in @section_owner

    assert_difference('Post.count', -1) do
      delete :destroy, section_id: @public_section, id: @public_section_post_by_section_owner
    end

    assert_redirected_to section_path(@public_section)
  end

  test 'public - section owner should destroy post if not owner' do
    sign_in @section_owner

    assert_difference('Post.count', -1) do
      delete :destroy, section_id: @public_section, id: @public_section_post_by_regular_user_one
    end

    assert_redirected_to section_path(@public_section)
  end

  test 'public - section owner should not vote up post if owner' do
    sign_in @section_owner

    assert_no_difference('@public_section_post_by_section_owner.plusminus') do
      post :vote_up, section_id: @public_section, id: @public_section_post_by_section_owner
    end

    assert_redirected_to section_post_path(@public_section, @public_section_post_by_section_owner)
  end

  test 'public - section owner should vote up post if not owner' do
    sign_in @section_owner

    assert_difference('@public_section_post_by_regular_user_one.plusminus') do
      post :vote_up, section_id: @public_section, id: @public_section_post_by_regular_user_one
    end

    assert_redirected_to section_post_path(@public_section, @public_section_post_by_regular_user_one)
  end

  test 'public - section collaborator should get new' do
    sign_in @section_collaborator_one

    get :new, section_id: @public_section

    assert_response :success
  end

  test 'public - section collaborator should create post' do
    sign_in @section_collaborator_one

    assert_difference('Post.count') do
      post :create, section_id: @public_section, post: { title: 'New Post Title', link_url: 'http://www.google.com/', content: 'New post content.' }
    end

    assert_redirected_to section_post_path(@public_section, assigns(:post))
  end

  test 'public - section collaborator should show post' do
    sign_in @section_collaborator_one

    get :show, section_id: @public_section, id: @public_section_post_by_regular_user_one

    assert_response :success
  end

  test 'public - section collaborator should get edit if owner' do
    sign_in @section_collaborator_one

    get :edit, section_id: @public_section, id: @public_section_post_by_section_collaborator_one

    assert_response :success
  end

  test 'public - section collaborator should not get edit if not owner' do
    sign_in @section_collaborator_one

    get :edit, section_id: @public_section, id: @public_section_post_by_regular_user_one

    assert_redirected_to section_post_path(@public_section, @public_section_post_by_regular_user_one)
  end

  test 'public - section collaborator should update post if owner' do
    sign_in @section_collaborator_one

    test_title = 'Updated Post Title'

    patch :update, section_id: @public_section, id: @public_section_post_by_section_collaborator_one, post: { title: test_title }

    assert_equal assigns(:post).title, test_title
    assert_redirected_to section_post_path(@public_section, assigns(:post))
  end

  test 'public - section collaborator should not update post if not owner' do
    sign_in @section_collaborator_one

    patch :update, section_id: @public_section, id: @public_section_post_by_regular_user_one, post: { title: 'Updated Post Title' }

    assert_equal assigns(:post).title, @public_section_post_by_regular_user_one.title
    assert_redirected_to section_post_path(@public_section, @public_section_post_by_regular_user_one)
  end

  test 'public - section collaborator should destroy post if owner' do
    sign_in @section_collaborator_one

    assert_difference('Post.count', -1) do
      delete :destroy, section_id: @public_section, id: @public_section_post_by_section_collaborator_one
    end

    assert_redirected_to section_path(@public_section)
  end

  test 'public - section collaborator should not destroy post if not owner' do
    sign_in @section_collaborator_one

    assert_no_difference('Post.count') do
      delete :destroy, section_id: @public_section, id: @public_section_post_by_regular_user_one
    end

    assert_redirected_to section_post_path(@public_section, @public_section_post_by_regular_user_one)
  end

  test 'public - section collaborator should not vote up post if owner' do
    sign_in @section_collaborator_one

    assert_no_difference('@public_section_post_by_section_collaborator_one.plusminus') do
      post :vote_up, section_id: @public_section, id: @public_section_post_by_section_collaborator_one
    end

    assert_redirected_to section_post_path(@public_section, @public_section_post_by_section_collaborator_one)
  end

  test 'public - section collaborator should vote up post if not owner' do
    sign_in @section_collaborator_one

    assert_difference('@public_section_post_by_regular_user_one.plusminus') do
      post :vote_up, section_id: @public_section, id: @public_section_post_by_regular_user_one
    end

    assert_redirected_to section_post_path(@public_section, @public_section_post_by_regular_user_one)
  end

  test 'public - regular user should get new' do
    sign_in @regular_user_two

    get :new, section_id: @public_section

    assert_response :success
  end

  test 'public - regular user should create post' do
    sign_in @regular_user_two

    assert_difference('Post.count') do
      post :create, section_id: @public_section, post: { title: 'New Post Title', link_url: 'http://www.google.com/', content: 'New Post Content' }
    end

    assert_redirected_to section_post_path(@public_section, assigns(:post))
  end

  test 'public - regular user should show post' do
    sign_in @regular_user_two

    get :show, section_id: @public_section, id: @public_section_post_by_regular_user_one

    assert_response :success
  end

  test 'public - regular user should get edit if owner' do
    sign_in @regular_user_one

    get :edit, section_id: @public_section, id: @public_section_post_by_regular_user_one

    assert_response :success
  end

  test 'public - regular user should not get edit if not owner' do
    sign_in @regular_user_two

    get :edit, section_id: @public_section, id: @public_section_post_by_regular_user_one

    assert_redirected_to section_post_path(@public_section, @public_section_post_by_regular_user_one)
  end

  test 'public - regular user should update post if owner' do
    sign_in @regular_user_one

    test_title = 'Updated Post Title'

    patch :update, section_id: @public_section, id: @public_section_post_by_regular_user_one, post: { title: test_title }

    assert_equal assigns(:post).title, test_title
    assert_redirected_to section_post_path(@public_section, @public_section_post_by_regular_user_one)
  end

  test 'public - regular user should not update post if not owner' do
    sign_in @regular_user_two

    patch :update, section_id: @public_section, id: @public_section_post_by_regular_user_one, post: { title: 'Updated Post Title' }

    assert_equal assigns(:post).title, @public_section_post_by_regular_user_one.title
    assert_redirected_to section_post_path(@public_section, @public_section_post_by_regular_user_one)
  end

  test 'public - regular user should destroy post if owner' do
    sign_in @regular_user_one

    assert_difference('Post.count', -1) do
      delete :destroy, section_id: @public_section, id: @public_section_post_by_regular_user_one
    end

    assert_redirected_to section_path(@public_section)
  end

  test 'public - regular user should not destroy post if not owner' do
    sign_in @regular_user_two

    assert_no_difference('Post.count') do
      delete :destroy, section_id: @public_section, id: @public_section_post_by_regular_user_one
    end

    assert_redirected_to section_post_path(@public_section, @public_section_post_by_regular_user_one)
  end

  test 'public - regular user should not vote up post if owner' do
    sign_in @regular_user_one

    assert_no_difference('@public_section_post_by_regular_user_one.plusminus') do
      post :vote_up, section_id: @public_section, id: @public_section_post_by_regular_user_one
    end

    assert_redirected_to section_post_path(@public_section, @public_section_post_by_regular_user_one)
  end

  test 'public - regular user should vote up post if not owner' do
    sign_in @regular_user_two

    assert_difference('@public_section_post_by_regular_user_one.plusminus') do
      post :vote_up, section_id: @public_section, id: @public_section_post_by_regular_user_one
    end

    assert_redirected_to section_post_path(@public_section, @public_section_post_by_regular_user_one)
  end

  # private section

  test 'private - section owner should get new' do
    sign_in @section_owner

    get :new, section_id: @private_section

    assert_response :success
  end

  test 'private - section owner should create post' do
    sign_in @section_owner

    assert_difference('Post.count') do
      post :create, section_id: @private_section, post: { title: 'New Post Title', link_url: 'http://www.google.com/', content: 'New post content.' }
    end

    assert_redirected_to section_post_path(@private_section, assigns(:post))
  end

  test 'private - section owner should show post' do
    sign_in @section_owner

    get :show, section_id: @private_section, id: @private_section_post_by_section_collaborator_one

    assert_response :success
  end

  test 'private - section owner should get edit if owner' do
    sign_in @section_owner

    get :edit, section_id: @private_section, id: @private_section_post_by_section_owner

    assert_response :success
  end

  test 'private - section owner should get edit if not owner' do
    sign_in @section_owner

    get :edit, section_id: @private_section, id: @private_section_post_by_section_collaborator_one

    assert_response :success
  end

  test 'private - section owner should update post if owner' do
    sign_in @section_owner

    test_title = 'Updated Post Title'

    patch :update, section_id: @private_section, id: @private_section_post_by_section_owner, post: { title: test_title }

    assert_equal assigns(:post).title, test_title
    assert_redirected_to section_post_path(@private_section, assigns(:post))
  end

  test 'private - section owner should update post if not owner' do
    sign_in @section_owner

    test_title = 'Updated Post Title'

    patch :update, section_id: @private_section, id: @private_section_post_by_section_collaborator_one, post: { title: test_title }

    assert_equal assigns(:post).title, test_title
    assert_redirected_to section_post_path(@private_section, assigns(:post))
  end

  test 'private - section owner should destroy post if owner' do
    sign_in @section_owner

    assert_difference('Post.count', -1) do
      delete :destroy, section_id: @private_section, id: @private_section_post_by_section_owner
    end

    assert_redirected_to section_path(@private_section)
  end

  test 'private - section owner should destroy post if not owner' do
    sign_in @section_owner

    assert_difference('Post.count', -1) do
      delete :destroy, section_id: @private_section, id: @private_section_post_by_section_collaborator_one
    end

    assert_redirected_to section_path(@private_section)
  end

  test 'private - section owner should not vote up post if owner' do
    sign_in @section_owner

    assert_no_difference('@private_section_post_by_section_owner.plusminus') do
      post :vote_up, section_id: @private_section, id: @private_section_post_by_section_owner
    end

    assert_redirected_to section_post_path(@private_section, @private_section_post_by_section_owner)
  end

  test 'private - section owner should vote up post if not owner' do
    sign_in @section_owner

    assert_difference('@private_section_post_by_section_collaborator_one.plusminus') do
      post :vote_up, section_id: @private_section, id: @private_section_post_by_section_collaborator_one
    end

    assert_redirected_to section_post_path(@private_section, @private_section_post_by_section_collaborator_one)
  end

  test 'private - section collaborator should get new' do
    sign_in @section_collaborator_one

    get :new, section_id: @private_section

    assert_response :success
  end

  test 'private - section collaborator should create post' do
    sign_in @section_collaborator_one

    assert_difference('Post.count') do
      post :create, section_id: @private_section, post: { title: 'New Post Title', link_url: 'http://www.google.com/', content: 'New post content.' }
    end

    assert_redirected_to section_post_path(@private_section, assigns(:post))
  end

  test 'private - section collaborator should show post' do
    sign_in @section_collaborator_one

    get :show, section_id: @private_section, id: @private_section_post_by_section_owner

    assert_response :success
  end

  test 'private - section collaborator should get edit if owner' do
    sign_in @section_collaborator_one

    get :edit, section_id: @private_section, id: @private_section_post_by_section_collaborator_one

    assert_response :success
  end

  test 'private - section collaborator should not get edit if not owner' do
    sign_in @section_collaborator_one

    get :edit, section_id: @private_section, id: @private_section_post_by_section_owner

    assert_redirected_to section_post_path(@private_section, @private_section_post_by_section_owner)
  end

  test 'private - section collaborator should update post if owner' do
    sign_in @section_collaborator_one

    test_title = 'Update Post Title'

    patch :update, section_id: @private_section, id: @private_section_post_by_section_collaborator_one, post: { title: test_title }

    assert_equal assigns(:post).title, test_title
    assert_redirected_to section_post_path(@private_section, assigns(:post))
  end

  test 'private - section collaborator should not update post if not owner' do
    sign_in @section_collaborator_one

    patch :update, section_id: @private_section, id: @private_section_post_by_section_owner, post: { title: 'Updated Post Title' }

    assert_equal assigns(:post).title, @private_section_post_by_section_owner.title
    assert_redirected_to section_post_path(@private_section, @private_section_post_by_section_owner)
  end

  test 'private - section collaborator should destroy post if owner' do
    sign_in @section_collaborator_one

    assert_difference('Post.count', -1) do
      delete :destroy, section_id: @private_section, id: @private_section_post_by_section_collaborator_one
    end

    assert_redirected_to section_path(@private_section)
  end

  test 'private - section collaborator should not destroy post if not owner' do
    sign_in @section_collaborator_one

    assert_no_difference('Post.count') do
      delete :destroy, section_id: @private_section, id: @private_section_post_by_section_owner
    end

    assert_redirected_to section_post_path(@private_section, @private_section_post_by_section_owner)
  end

  test 'private - section collaborator should not vote up post if owner' do
    sign_in @section_collaborator_one

    assert_no_difference('@private_section_post_by_section_collaborator_one.plusminus') do
      post :vote_up, section_id: @private_section, id: @private_section_post_by_section_collaborator_one
    end

    assert_redirected_to section_post_path(@private_section, @private_section_post_by_section_collaborator_one)
  end

  test 'private - section collaborator should vote up post if not owner' do
    sign_in @section_collaborator_one

    assert_difference('@private_section_post_by_section_owner.plusminus') do
      post :vote_up, section_id: @private_section, id: @private_section_post_by_section_owner
    end

    assert_redirected_to section_post_path(@private_section, @private_section_post_by_section_owner)
  end

  test 'private - regular user should not get new' do
    sign_in @regular_user_one

    get :new, section_id: @private_section

    assert_redirected_to root_path
  end

  test 'private - regular user should not create post' do
    sign_in @regular_user_one

    assert_no_difference('Post.count') do
      post :create, section_id: @private_section, post: { title: 'New Post Title', link_url: 'http://www.google.com/', content: 'New post content.' }
    end

    assert_redirected_to root_path
  end

  test 'private - regular user should not show post' do
    sign_in @regular_user_one

    get :show, section_id: @private_section, id: @private_section_post_by_section_collaborator_one

    assert_redirected_to root_path
  end

  test 'private - regular user should not get edit' do
    sign_in @regular_user_one

    get :edit, section_id: @private_section, id: @private_section_post_by_section_collaborator_one

    assert_redirected_to root_path
  end

  test 'private - regular user should not update post' do
    sign_in @regular_user_one

    patch :update, section_id: @private_section, id: @private_section_post_by_section_collaborator_one, post: { title: 'Updated Post Title' }

    assert_redirected_to root_path
  end

  test 'private - regular user should not destroy post' do
    sign_in @regular_user_one

    assert_no_difference('Post.count') do
      delete :destroy, section_id: @private_section, id: @private_section_post_by_section_collaborator_one
    end

    assert_redirected_to root_path
  end

  test 'private - regular user should not vote up post' do
    sign_in @regular_user_one

    assert_no_difference('@private_section_post_by_section_collaborator_one.plusminus') do
      post :vote_up, section_id: @private_section, id: @private_section_post_by_section_collaborator_one
    end

    assert_redirected_to root_path
  end

  test 'should recalculate post score after vote' do
    sign_in @regular_user_two

    post_score_before = @public_section_post_by_regular_user_one.score

    assert_difference('Delayed::Job.count') do
      post :vote_up, section_id: @public_section, id: @public_section_post_by_regular_user_one
    end

    # process delayed job
    Delayed::Worker.new.work_off

    # reload comment to get updated score
    @public_section_post_by_regular_user_one.reload
    post_score_after = @public_section_post_by_regular_user_one.score

    assert_operator post_score_after, :>, post_score_before
  end
end