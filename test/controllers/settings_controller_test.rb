require 'test_helper'

class SettingsControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  setup do
    @regular_user_one = users(:regular_user_one)
  end

  test 'all users should get settings profile' do
    sign_in @regular_user_one

    get :profile

    assert_response :success
  end
end
