require 'test_helper'

class RegistrationsControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  setup do
    # ensure registrations controller sub-class is used for tests
    request.env['devise.mapping'] = Devise.mappings[:user]

    @regular_user_one = users(:regular_user_one)
  end

  test 'should redirect to user path after update' do
    sign_in @regular_user_one

    put :update, user: { current_password: 'password', password: 'newpassword', password_confirmation: 'newpassword' }

    assert_redirected_to user_path(@regular_user_one)
  end

  test 'should raise RecordNotFound and redirect to sign in path after account cancelled' do
    sign_in @regular_user_one

    delete :destroy

    assert_raises ActiveRecord::RecordNotFound do
      User.find(@regular_user_one.id)
    end

    assert_redirected_to root_path
  end
end