require 'test_helper'

class SectionsControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  setup do
    @section_owner = users(:section_owner)
    @section_collaborator_one = users(:section_collaborator_one)
    @regular_user_one = users(:regular_user_one)
    
    @public_section = sections(:public_section)
    @private_section = sections(:private_section)
  end

  test 'all users should get index' do
    sign_in @regular_user_one

    get :index

    assert_response :success
  end

  test 'all users should get new' do
    sign_in @regular_user_one

    get :new

    assert_response :success
  end

  test 'all users should create section' do
    sign_in @regular_user_one

    assert_difference('Section.count') do
      post :create, section: { title: 'New Section Title', private: false }
    end

    assert_redirected_to section_path(assigns(:section))
  end

  # public section

  test 'public - section owner should show section' do
    sign_in @section_owner

    get :show, id: @public_section

    assert_response :success
  end

  test  'public - section owner should get edit' do
    sign_in @section_owner

    get :edit, id: @public_section

    assert_response :success
  end

  test 'public - section owner should update section' do
    sign_in @section_owner

    test_title = 'Updated Section Title'

    patch :update, id: @public_section, section: { title: test_title }

    assert_equal assigns(:section).title, test_title
    assert_redirected_to edit_section_path(assigns(:section))
  end

  test 'public - section owner should destroy section' do
    sign_in @section_owner

    assert_difference('Section.count', -1) do
      delete :destroy, id: @public_section
    end

    assert_redirected_to root_path
  end

  test 'public section collaborator should show section' do
    sign_in @section_collaborator_one

    get :show, id: @public_section

    assert_response :success
  end

  test 'public - section collaborator should not get edit' do
    sign_in @section_collaborator_one

    get :edit, id: @public_section

    assert_redirected_to section_path(@public_section)
  end

  test 'public - section collaborator should not update section' do
    sign_in @section_collaborator_one

    patch :update, id: @public_section, section: 'Updated Section Title'

    assert_equal @public_section.title, assigns(:section).title
    assert_redirected_to section_path(@public_section)
  end

  test 'public - section collaborator should not destroy section' do
    sign_in @section_collaborator_one

    assert_no_difference('Section.count') do
      delete :destroy, id: @public_section
    end

    assert_redirected_to section_path(@public_section)
  end

  test 'public - regular user should show section' do
    sign_in @regular_user_one

    get :show, id: @public_section

    assert_response :success
  end

  test 'public - regular user should not get edit' do
    sign_in @regular_user_one

    get :edit, id: @public_section

    assert_redirected_to section_path(@public_section)
  end

  test 'public - regular user should not update section' do
    sign_in @regular_user_one

    patch :update, id: @public_section, section: { title: 'Updated Section Title' }

    assert_equal @public_section.title, assigns(:section).title
    assert_redirected_to section_path(@public_section)
  end

  test 'public - regular user should not destroy section' do
    sign_in @regular_user_one

    assert_no_difference('Section.count') do
      delete :destroy, id: @public_section
    end

    assert_redirected_to section_path(@public_section)
  end

  # private section

  test 'private - section owner should show section' do
    sign_in @section_owner

    get :show, id: @private_section

    assert_response :success
  end

  test 'private - section owner should get edit' do
    sign_in @section_owner

    get :edit, id: @private_section

    assert_response :success
  end

  test 'private - section owner should update section' do
    sign_in @section_owner

    test_title = 'Updated Section Title'

    patch :update, id: @private_section, section: { title: test_title }

    assert_equal assigns(:section).title, test_title
    assert_redirected_to edit_section_path(assigns(:section))
  end

  test 'private - section owner should destroy section' do
    sign_in @section_owner

    delete :destroy, id: @private_section

    assert_redirected_to root_path
  end

  test 'private - section collaborator should show section' do
    sign_in @section_collaborator_one

    get :show, id: @private_section

    assert_response :success
  end

  test 'private - section collaborator should not get edit' do
    sign_in @section_collaborator_one

    get :edit, id: @private_section

    assert_redirected_to section_path(@private_section)
  end

  test 'private - section collaborator should not update section' do
    sign_in @section_collaborator_one

    patch :update, id: @private_section, section: { title: 'Updated Section Title' }

    assert_equal @private_section.title, assigns(:section).title
    assert_redirected_to section_path(@private_section)
  end

  test 'private - section collaborator should not destroy section' do
    sign_in @section_collaborator_one

    assert_no_difference('Section.count') do
      delete :destroy, id: @private_section
    end

    assert_redirected_to section_path(@private_section)
  end

  test 'private - regular user should not show section' do
    sign_in @regular_user_one

    get :show, id: @private_section

    assert_redirected_to root_path
  end

  test 'private - regular user should not get edit' do
    sign_in @regular_user_one

    get :edit, id: @private_section

    assert_redirected_to root_path
  end

  test 'private - regular user should not update section' do
    sign_in @regular_user_one

    patch :update, id: @private_section, section: { title: 'Updated Section Title' }

    assert_equal @private_section.title, assigns(:section).title
    assert_redirected_to root_path
  end

  test 'private - regular user should not destroy section' do
    sign_in @regular_user_one

    assert_no_difference('Section.count') do
      delete :destroy, id: @private_section
    end

    assert_redirected_to root_path
  end
end