require 'test_helper'

class CommentsControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  setup do
    @section_owner = users(:section_owner)
    @section_collaborator_one = users(:section_collaborator_one)
    @regular_user_one = users(:regular_user_one)
    @regular_user_two = users(:regular_user_two)

    @public_section = sections(:public_section)
    @private_section = sections(:private_section)

    @public_section_post_by_section_owner = posts(:public_section_post_by_section_owner)
    @public_section_post_by_regular_user_one = posts(:public_section_post_by_regular_user_one)
    @private_section_post_by_section_collaborator_one = posts(:private_section_post_by_section_collaborator_one)

    @public_section_comment_by_regular_user_one = comments(:public_section_comment_by_regular_user_one)
    @private_section_comment_by_section_collaborator_two = comments(:private_section_comment_by_section_collaborator_two)
  end

  # public section

  test 'public - section owner should create comment' do
    sign_in @section_owner

    assert_difference('Comment.count') do
      post :create, section_id: @public_section, post_id: @public_section_post_by_regular_user_one, comment: { content: 'Test content.' }
    end

    assert_redirected_to section_post_path(@public_section, @public_section_post_by_regular_user_one)
  end

  test 'public - section owner should vote up comment' do
    sign_in @section_owner

    assert_difference('@public_section_comment_by_regular_user_one.plusminus') do
      post :vote_up, section_id: @public_section, post_id: @public_section_post_by_section_owner, comment_id: @public_section_comment_by_regular_user_one
    end

    assert_redirected_to section_post_path(@public_section, @public_section_post_by_section_owner)
  end

  test 'public - section collaborator should create comment' do
    sign_in @section_collaborator_one

    assert_difference('Comment.count') do
      post :create, section_id: @public_section, post_id: @public_section_post_by_regular_user_one, comment: { content: 'Test content.' }
    end

    assert_redirected_to section_post_path(@public_section, @public_section_post_by_regular_user_one)
  end

  test 'public - section collaborator should vote up comment' do
    sign_in @section_collaborator_one

    assert_difference('@public_section_comment_by_regular_user_one.plusminus') do
      post :vote_up, section_id: @public_section, post_id: @public_section_post_by_section_owner, comment_id: @public_section_comment_by_regular_user_one
    end

    assert_redirected_to section_post_path(@public_section, @public_section_post_by_section_owner)
  end

  test 'public - regular user should create comment' do
    sign_in @regular_user_two

    assert_difference('Comment.count') do
      post :create, section_id: @public_section, post_id: @public_section_post_by_regular_user_one, comment: { content: 'Test content.' }
    end

    assert_redirected_to section_post_path(@public_section, @public_section_post_by_regular_user_one)
  end

  test 'public - regular user should vote up comment' do
    sign_in @regular_user_two

    assert_difference('@public_section_comment_by_regular_user_one.plusminus') do
      post :vote_up, section_id: @public_section, post_id: @public_section_post_by_section_owner, comment_id: @public_section_comment_by_regular_user_one
    end

    assert_redirected_to section_post_path(@public_section, @public_section_post_by_section_owner)
  end

  # private section

  test 'private - section owner should create comment' do
    sign_in @section_owner

    assert_difference('Comment.count') do
      post :create, section_id: @private_section, post_id: @private_section_post_by_section_collaborator_one, comment: { content: 'Test content.' }
    end

    assert_redirected_to section_post_path(@private_section, @private_section_post_by_section_collaborator_one)
  end

  test 'private - section owner should vote up comment' do
    sign_in @section_owner

    assert_difference('@private_section_comment_by_section_collaborator_two.plusminus') do
      post :vote_up, section_id: @private_section, post_id: @private_section_post_by_section_collaborator_one, comment_id: @private_section_comment_by_section_collaborator_two
    end

    assert_redirected_to section_post_path(@private_section, @private_section_post_by_section_collaborator_one)
  end

  test 'private - section collaborator should create comment' do
    sign_in @section_collaborator_one

    assert_difference('Comment.count') do
      post :create, section_id: @private_section, post_id: @private_section_post_by_section_collaborator_one, comment: { content: 'Test content.' }
    end

    assert_redirected_to section_post_path(@private_section, @private_section_post_by_section_collaborator_one)
  end

  test 'private - section collaborator should vote up comment' do
    sign_in @section_collaborator_one

    assert_difference('@private_section_comment_by_section_collaborator_two.plusminus') do
      post :vote_up, section_id: @private_section, post_id: @private_section_post_by_section_collaborator_one, comment_id: @private_section_comment_by_section_collaborator_two
    end

    assert_redirected_to section_post_path(@private_section, @private_section_post_by_section_collaborator_one)
  end

  test 'private - regular user should not create comment' do
    sign_in @regular_user_one

    assert_no_difference('Comment.count') do
      post :create, section_id: @private_section, post_id: @private_section_comment_by_section_collaborator_two, comment: { content: 'Test content.' }
    end

    assert_redirected_to root_path
  end

  test 'private - regular user should not vote up comment' do
    sign_in @regular_user_one

    assert_no_difference('@private_section_comment_by_section_collaborator_two.plusminus') do
      post :vote_up, section_id: @private_section, post_id: @private_section_post_by_section_collaborator_one, comment_id: @private_section_comment_by_section_collaborator_two
    end

    assert_redirected_to root_path
  end

  test 'should recalculate comment score after vote' do
    sign_in @regular_user_two

    comment_score_before = @public_section_comment_by_regular_user_one.score

    assert_difference('Delayed::Job.count') do
      post :vote_up, section_id: @public_section, post_id: @public_section_post_by_section_owner, comment_id: @public_section_comment_by_regular_user_one
    end

    # process delayed job
    Delayed::Worker.new.work_off

    # reload comment to get updated score
    @public_section_comment_by_regular_user_one.reload
    comment_score_after = @public_section_comment_by_regular_user_one.score

    assert_operator comment_score_after, :>, comment_score_before
  end
end