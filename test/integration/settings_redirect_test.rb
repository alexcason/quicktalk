require 'test_helper'

class AdminSettingsRedirectTest < ActionDispatch::IntegrationTest
  setup do
    @regular_user_one = users(:regular_user_one)
  end

  test 'settings root should redirect to settings profile' do
    login(@regular_user_one)

    get_via_redirect settings_path

    assert_response :success
    assert_equal profile_settings_path, path
  end

  test 'should get settings admin' do
    login(@regular_user_one)

    get_via_redirect admin_settings_path

    assert_response :success
    assert_equal admin_settings_path, path
  end

  private
    def login(user)
      # navigate to sign in page
      get_via_redirect new_user_session_path
      assert_response :success

      # sign in as provided user
      post_via_redirect user_session_path, user: { email: user.email, password: 'password', remember_me: false }
      assert_equal '/', path
    end
end